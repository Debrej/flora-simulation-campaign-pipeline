import yaml
from os import listdir
from os.path import isfile, join
from jinja2 import Template

print("\nGENERATING SIMULATIONS TEMPLATES FROM SIMULATIONS SOURCES...")

simulations_sources_directory = "simulations_sources/"
simulations_output_directory = "simulations/"
template_file = "templates/simulation.j2"

simulation_sources_files = [
    f for f in listdir(simulations_sources_directory)
    if isfile(join(simulations_sources_directory, f))
]

print(f"GENERATING {len(simulation_sources_files)} SIMULATIONS TEMPLATES...")

for sim in simulation_sources_files:
    simulation_name = sim[:-4]
    print(simulation_name)
    with open(join(simulations_sources_directory, sim)) as file:
        simulation = yaml.safe_load(file)

    with open(template_file) as file:
        template = Template(file.read())

    with open(join(simulations_output_directory, f"{simulation_name}.ini"),
              "w+") as file:
        file.write(template.render(simulation))

print("END OF GENERATION\n")