#!/bin/bash

mkdir -p tmp;

python3 setup.py;
source tmp/setup_variable.sh;

python3 $CODE_FOLDER_PATH/data_extraction.py;
python3 $CODE_FOLDER_PATH/data_visualization.py;

timestamp=$(date +%s);
tar czvf $CODE_FOLDER_PATH/backup/images/images_$timestamp.tar.gz $CODE_FOLDER_PATH/images/*;

python3 $CODE_FOLDER_PATH/report_generation.py;
