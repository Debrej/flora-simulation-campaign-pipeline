# FLoRa Simulation Campaign Pipeline
These Python scripts are used to run simulations campaign using FLoRa simulator on Omnet++.

## Versions
The following versions are used :
+ __Ubuntu__: _Ubuntu 20.04 LTS_
+ __Python__: _Python 3.8.10_
+ __FLoRa__: _flora-1.0.0_
+ __Omnet++__: _omnetpp-6.0pre11_

## Getting started
If the versions of software are correct on your system and you want to try, you need to perform the following steps:
### Cloning the repo
```bash
git clone https://gitlab.com/Debrej/flora-simulation-campaign-pipeline
```
### Changing the setup variables
Head to `config/setup.yml` and change the values according to your installation:
```yaml
code_folder: /home/user/code/flora-simulation-campaign-pipeline # replace with your code folder path
omnetpp_folder: /home/user/omnetpp-6.0pre11 # replace with your Omnet++ folder base path
```
### Changing the simulations variables
Head to `config/simulations.yml` and change the values according to your liking:
```yaml
numberOfNodes: # an array of the number of nodes in each simulation
  - 10
  - 15

activateADR: false # if the simulation implements ADR
```
### Launch the campaign
Run the following command to launch the campaign:
```bash
./campaign.sh
```
### After the campaign
Once the simulations have runned their courses: 
+ the logs are available in `logs/`
+ the extracted data in `data/simulations.yml`
+ the produced graphs in `images/`
+ the recap in `out/campaign_<timestamp>.pdf`
