import os
import stat
import subprocess, yaml
from jinja2 import Template

print("\nRUNNING SIMULATIONS...")

simulations_config = yaml.safe_load(open("tmp/simulations.yml"))
template_file = "templates/run_simulations.j2"
run_file = "tmp/run_simulations.sh"

print(f"RUNNING {len(simulations_config)} SIMULATIONS...")

sim_names = [f"{sim['simulationName']}.ini" for sim in simulations_config]

with open(template_file) as file:
    template = Template(file.read())

with open(run_file, "w") as file:
    file.write(template.render(simulations=sim_names))

os.chmod(run_file, stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)

output =subprocess.check_output("/home/thibaut/code/pfe/tmp/run_simulations.sh",
                        encoding="UTF-8")

print(output)

print("END OF SIMULATIONS RUNNING.\n")
