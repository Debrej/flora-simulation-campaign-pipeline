import datetime, os, yaml
from jinja2 import Template

PLOT_TYPES = ["pdr-map", "pdr-slice", "sf-map", "slice-map"]
GEN_PLOT_TYPES = ["pdr-evolution"]


class PdfTemplate():
    def __init__(self, figs, sims=[], toc=True):
        self.sims = [{"name": sim["simulationName"], "plots": []} for sim in sims]
        for sim in self.sims:
            sim["plots"].extend([fig for fig in figs if sim["name"] in fig])
            for plot_type in PLOT_TYPES:
                sim[plot_type] = [fig for fig in sim["plots"] if plot_type in fig][0]
        self.gen_plots = {"pdr-evolution": [f for f in figs if "pdr-evolution" in f][0]}
        self.toc = toc
        self.text = []

    def render(self):
        #region INTRO
        self._pagebreak()
        with open("tmp/report_introduction.md", "r") as f:
            self.text.append(f.read())
        #endregion
        #region GENERAL PLOTS
        self._pagebreak()
        self._h1("General plots")
        for plot_type in GEN_PLOT_TYPES:
            self._h2(plot_type)
            self._img(os.path.join("images", self.gen_plots[plot_type]))
        #endregion
        #region INDIVIDUAL PLOTS
        self._pagebreak()
        self._h1("Individual plots")
        for plot_type in PLOT_TYPES:
            self._h2(plot_type)
            for sim in self.sims:
                self._h3(sim["name"])
                self._img(os.path.join("images", sim[plot_type]))
                self._pagebreak()
        #endregion
        self.text = "\n\n".join(self.text)

    def export(self):
        dt = datetime.datetime.now()
        date = str(dt).replace(" ", "_").replace(":", "_").replace(
            ".", "_").replace("-", "_")
        md_file = f"out/campaign_{date}.md"
        pdf_file = f"out/campaign_{date}.pdf"
        pandoc = ["pandoc", f"{md_file}", f"-o {pdf_file}"]
        with open(md_file, "w") as f:
            f.write(self.text)
        if self.toc:
            pandoc.append("--toc")
        os.system(" ".join(pandoc))
        os.remove(md_file)

    def _pagebreak(self):
        self.text.append("\pagebreak")

    def _h1(self, text):
        self.text.append(f"# {text}")

    def _h2(self, text):
        self.text.append(f"## {text}")

    def _h3(self, text):
        self.text.append(f"### {text}")

    def _h4(self, text):
        self.text.append(f"#### {text}")

    def _h5(self, text):
        self.text.append(f"##### {text}")

    def _img(self, img):
        self.text.append(f"![]({img})")


print("\nGENERATING REPORT...")

print("GENERATING MARKDOWN FILE...")
sims = yaml.safe_load(open("tmp/simulations.yml"))
template_file = "templates/report_introduction.j2"

with open(template_file) as file:
    template = Template(file.read())

with open("tmp/report_introduction.md", "w+") as file:
    file.write(template.render(simulations=sims))

print("REDENRING PDF...")
pdf = PdfTemplate(figs=os.listdir("images"), toc=False, sims=sims)
pdf.render()
pdf.export()

print("REPORT GENERATED.\n")