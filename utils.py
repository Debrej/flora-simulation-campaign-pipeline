class BOUNDARIES:
    class NODE_BOUNDARIES:
        X = [320,380]
        Y = [320,380]

    class GW_BOUNDARIES:
        X = [340,360]
        Y = [340,360]

    class NS_BOUNDARIES:
        X = [340,360]
        Y = [340,360]

class SLICES:
    types = ["zombie", "normal", "reliable"]
    class ZOMBIE:
        SF = [11]

    class NORMAL:
        SF = [8,9,10,12]

    class RELIABLE:
        SF = [7]