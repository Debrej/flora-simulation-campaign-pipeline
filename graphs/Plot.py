import re
import yaml
import matplotlib.pyplot as plt
from matplotlib import ticker
import numpy as np

from utils import SLICES

plt.style.use("seaborn")

red = "#F49C73"
blue = "#58CFEE"
gateway = "#8D5ED4"


class PDR_BY_SLICE_BOXPLOT:
    def __init__(self, data, source, std_on_nodes=True):
        slices_pdr = {"zombie": [], "normal": [], "reliable": []}
        all_nodes_pdr = {"zombie": [], "normal": [], "reliable": []}
        for sim_i in range(source["config"]["numberOfRepeat"]):
            sim_slices_pdr = {"zombie": [], "normal": [], "reliable": []}
            for num in range(data["nb_n"]):
                pdr = (data["packetReceivedByNodeAll"]
                       [f"packetReceivedByNodeAll_{num}"][sim_i] /
                       data["packetSentByNodeAll"]
                       [f"packetSentByNodeAll_{num}"][sim_i]) * 100
                sim_slices_pdr[data["initialSlice"]
                               [f"initialSlice_{num}"]].append(pdr)
                all_nodes_pdr[data["initialSlice"]
                              [f"initialSlice_{num}"]].append(pdr)
            for s in SLICES.types:
                slices_pdr[s].append(float(np.array(sim_slices_pdr[s]).mean()))

        slices_data_mean = []
        slices_data_std = []

        for s in SLICES.types:
            if not std_on_nodes:
                slices_data_std.append(np.array(slices_pdr[s]).std())
            else:
                slices_data_std.append(np.array(all_nodes_pdr[s]).std())
            slices_data_mean.append(np.array(slices_pdr[s]).mean())

        fig, ax = plt.subplots(figsize=(8, 8), dpi=300)

        ax.bar(SLICES.types,
               slices_data_mean,
               color=blue,
               label="Packet delivering ratio",
               yerr=slices_data_std)

        ax.set_xlabel("Slices")
        ax.set_ylabel("PDR")
        ax.set_ylim(0, 100)
        ax.tick_params(axis='x', rotation=45)

        slicing = " with slicing" if source["config"][
            "sliceUse"] else " without slicing"

        nb_n = source["config"]["numberOfNodes"]
        nb_g = source["config"]["numberOfGateways"]
        z_r = source["config"]["zombieNodesPercentage"]
        r_r = source["config"]["reliableNodesPercentage"]

        all_nodes = "all nodes" if std_on_nodes else "all simulations"

        ax.set_title(
            f"Packets delivering ratio by slice type{slicing}, error on {all_nodes}\nn{nb_n} g{nb_g} z{z_r} r{r_r}"
        )

        fig.tight_layout()
        self.fig = fig


class PDR_BY_ZOMBIE_NODE_PERCENTAGE:
    def __init__(self, data):
        #region DATA PREPARATION
        sim_sources = yaml.safe_load(open("tmp/simulations.yml"))

        zombie_node_regex = re.compile(".*z(?P<value>[0-9]+).*")
        zombie_node_pct = list(
            dict.fromkeys([
                zombie_node_regex.match(d["name"]).group("value") for d in data
            ]))

        pdr_data = {}

        for pct in zombie_node_pct:
            pdr_data[pct] = {
                "zombie": {
                    "slicing": [],
                    "no_slicing": []
                },
                "normal": {
                    "slicing": [],
                    "no_slicing": []
                },
                "reliable": {
                    "slicing": [],
                    "no_slicing": []
                }
            }
            slicing_sims = [
                d["simulationName"] for d in sim_sources
                if d["sliceUse"] and d["zombieNodesPercentage"] == float(pct) /
                100
            ]
            no_slicing_sims = [
                d["simulationName"] for d in sim_sources if not d["sliceUse"]
                and d["zombieNodesPercentage"] == float(pct) / 100
            ]

            slicing_data = [d for d in data if d["name"] in slicing_sims]
            no_slicing_data = [d for d in data if d["name"] in no_slicing_sims]

            for d in slicing_data:
                for num in range(d["nb_n"]):
                    pdr = (
                        d["packetReceivedByNode"]
                        [f"packetReceivedByNode_{num}"] /
                        d["packetSentByNode"][f"packetSentByNode_{num}"]) * 100
                    pdr_data[pct][d["initialSlice"][f"initialSlice_{num}"]][
                        "slicing"].append(pdr)
            for d in no_slicing_data:
                for num in range(d["nb_n"]):
                    pdr = (
                        d["packetReceivedByNode"]
                        [f"packetReceivedByNode_{num}"] /
                        d["packetSentByNode"][f"packetSentByNode_{num}"]) * 100
                    pdr_data[pct][d["initialSlice"][f"initialSlice_{num}"]][
                        "no_slicing"].append(pdr)

            for sliceUse in ["slicing", "no_slicing"]:
                for s in SLICES.types:
                    pdr_data[pct][s][sliceUse] = float(
                        np.array(pdr_data[pct][s][sliceUse]).mean())
                    if np.isnan(pdr_data[pct][s][sliceUse]):
                        pdr_data[pct][s][sliceUse] = 0

        plot_data = {
            "slicing": {
                "zombie": [],
                "normal": [],
                "reliable": []
            },
            "no_slicing": {
                "zombie": [],
                "normal": [],
                "reliable": []
            }
        }

        for pct in zombie_node_pct:
            for s in SLICES.types:
                for sliceUse in ["slicing", "no_slicing"]:
                    plot_data[sliceUse][s].append(pdr_data[pct][s][sliceUse])
        #endregion

        #region PLOT

        x = np.arange(len(zombie_node_pct))
        width = 0.10

        fig, ax = plt.subplots(figsize=(8, 8), dpi=300)

        nb_bars_per_category = 6

        positions = [
            -3 * (width) + width / 2,
            -2 * (width) + width / 2,
            -1 * (width) + width / 2,
            1 * (width) - width / 2,
            2 * (width) - width / 2,
            3 * (width) - width / 2,
        ]
        colors = [
            "darkgrey", "lightgrey", "darkblue", "lightblue", "darkorange",
            "yellow"
        ]
        labels = [
            "Zombie / No Slicing",
            "Zombie / Slicing",
            "Normal / No Slicing",
            "Normal / Slicing",
            "Reliable / No Slicing",
            "Reliable / Slicing",
        ]
        dt = [
            ("no_slicing", "zombie"),
            ("slicing", "zombie"),
            ("no_slicing", "normal"),
            ("slicing", "normal"),
            ("no_slicing", "reliable"),
            ("slicing", "reliable"),
        ]

        for i in range(nb_bars_per_category):
            ax.bar_label(ax.bar(x + positions[i],
                                plot_data[dt[i][0]][dt[i][1]],
                                width,
                                color=colors[i],
                                label=labels[i]),
                         fmt="%.1f",
                         rotation="vertical",
                         padding=3,
                         fontsize="x-small")

        ax.legend(loc='center left',
                  bbox_to_anchor=(0, -0.2),
                  ncol=3,
                  frameon=False)

        ax.set_xticks(x, zombie_node_pct)

        ax.xaxis.set_label_text("Zombie node percentage")

        ax.yaxis.set_label_text("Packet delivery ratio")
        ax.yaxis.set_major_formatter(ticker.PercentFormatter())
        ax.set_ylim(0, 100)

        ax.set_title("Evolution of PDR by zombie node percentage")

        fig.tight_layout()

        #endregion

        self.fig = fig
