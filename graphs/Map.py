import matplotlib.pyplot as plt
from matplotlib.lines import Line2D

from utils import BOUNDARIES, SLICES

pdr_colors = {
    "m_75": "limegreen",
    "m_50": "yellow",
    "m_25": "darkorange",
    "m_0": "red",
    "zombie": "lightgrey"
}

sf_colors = {
    "sf_7": "limegreen",
    "sf_8": "green",
    "sf_9": "yellow",
    "sf_10": "darkorange",
    "sf_11": "red",
    "sf_12": "darkred",
}

slice_colors = {"normal": "dimgrey", "reliable": "darkgreen", "zombie": "red"}


def _color_coding(data, colors_grading, node_slices=None):
    colors = []

    if colors_grading == "pdr":
        for d in data:
            if d >= 75:
                pdr = 75
            elif d >= 50:
                pdr = 50
            elif d >= 25:
                pdr = 25
            else:
                pdr = 0
            colors.append(pdr_colors[f"m_{pdr}"])
        if node_slices is not None:
            for i in range(len(node_slices)):
                if node_slices[i] == "zombie":
                    colors[i] = pdr_colors["zombie"]

    elif colors_grading == "sf":
        for d in data:
            colors.append(sf_colors[f"sf_{d}"])

    elif colors_grading == "slices":
        for d in data:
            colors.append(slice_colors[d])

    return colors


slice_markers = {"zombie": "^", "normal": "H", "reliable": "P"}

plt.style.use("seaborn")

red = "#F49C73"
blue = "#58CFEE"
gateway = "#8D5ED4"


class PDR_MAP:
    def __init__(self, data, source):
        fig, ax = plt.subplots(figsize=(10, 10), dpi=300)

        #region DATA INIT
        nodes = {}
        for s in SLICES.types:
            nodes[s] = {
                "x": [
                    int(n['x'][:-4]) for n in source['loRaNodes']
                    if n['initialSlice'] == s
                ],
                "y": [
                    int(n['y'][:-4]) for n in source['loRaNodes']
                    if n['initialSlice'] == s
                ]
            }
        gw = {
            "x": [int(n['x'][:-4]) for n in source['gateways']],
            "y": [int(n['y'][:-4]) for n in source['gateways']]
        }

        #endregion
        #region DATA PLOTTING

        for s in SLICES.types:
            node_pdr = []
            node_slices = []

            for node in source["loRaNodes"]:
                if node["initialSlice"] == s:
                    rec = data["packetReceivedByNode"][
                        f"packetReceivedByNode_{node['num']}"]
                    sent = data["packetSentByNode"][
                        f"packetSentByNode_{node['num']}"]
                    node_pdr.append(rec / sent * 100)

                    node_slices.append(
                        data["initialSlice"][f"initialSlice_{node['num']}"])

            colors = _color_coding(node_pdr, "pdr", node_slices)

            ax.scatter(nodes[s]["x"],
                       nodes[s]["y"],
                       color=colors,
                       marker=slice_markers[s])

        ax.scatter(gw["x"], gw["y"], color=gateway, marker="s")

        #endregion
        #region LEGEND
        legend_elements = [
            Line2D([0], [0],
                   marker='s',
                   color=gateway,
                   label='LoRa Gateway',
                   ls=""),
            Line2D([0], [0],
                   marker=slice_markers["normal"],
                   color=pdr_colors["m_75"],
                   label='LoRa Node',
                   ls=""),
            Line2D([0], [0],
                   marker='o',
                   color=pdr_colors["m_75"],
                   label='PDR >= 75%',
                   ls=""),
            Line2D([0], [0],
                   marker='o',
                   color=pdr_colors["m_50"],
                   label='PDR >= 50% & < 75%',
                   ls=""),
            Line2D([0], [0],
                   marker='o',
                   color=pdr_colors["m_25"],
                   label='PDR >= 25% & < 50%',
                   ls=""),
            Line2D([0], [0],
                   marker='o',
                   color=pdr_colors["m_0"],
                   label='PDR < 25%',
                   ls=""),
            Line2D([0], [0],
                   marker=slice_markers["zombie"],
                   color=pdr_colors["zombie"],
                   label='Zombie node',
                   ls=""),
            Line2D([0], [0],
                   marker=slice_markers["reliable"],
                   color=pdr_colors["m_75"],
                   label='Reliable node',
                   ls=""),
        ]

        ax.legend(loc='center left',
                  bbox_to_anchor=(0, -0.2),
                  ncol=4,
                  frameon=False,
                  handles=legend_elements)

        #endregion
        #region SETTINGS

        ax.set_xlim(BOUNDARIES.NODE_BOUNDARIES.X[0] - 10,
                    BOUNDARIES.NODE_BOUNDARIES.X[1] + 10)
        ax.set_ylim(BOUNDARIES.NODE_BOUNDARIES.Y[0] - 10,
                    BOUNDARIES.NODE_BOUNDARIES.Y[1] + 10)

        slicing = " with slicing" if source["config"][
            "sliceUse"] else " without slicing"
        nb_n = source["config"]["numberOfNodes"]
        nb_g = source["config"]["numberOfGateways"]
        z_r = source["config"]["zombieNodesPercentage"]
        r_r = source["config"]["reliableNodesPercentage"]

        ax.set_title(
            f"Map of nodes and gateways{slicing}\nn{nb_n} g{nb_g} z{z_r} r{r_r}"
        )

        fig.tight_layout()
        self.fig = fig

        #endregion


class SF_MAP:
    def __init__(self, data, source):
        fig, ax = plt.subplots(figsize=(10, 10), dpi=300)
        nodes = {
            "x": [int(n['x'][:-4]) for n in source['loRaNodes']],
            "y": [int(n['y'][:-4]) for n in source['loRaNodes']]
        }
        gw = {
            "x": [int(n['x'][:-4]) for n in source['gateways']],
            "y": [int(n['y'][:-4]) for n in source['gateways']]
        }

        node_slices = []

        for node in source["loRaNodes"]:
            node_slices.append(data["finalSF"][f"finalSF_{node['num']}"])

        colors = _color_coding(node_slices, "sf")

        legend_elements = [
            Line2D([0], [0],
                   marker='s',
                   color=gateway,
                   label='LoRa Gateway',
                   ls=""), *[
                       Line2D([0], [0],
                              marker='o',
                              color=sf_colors[f"sf_{num}"],
                              label=f"SF {num}",
                              ls="") for num in [7, 8, 9, 10, 11, 12]
                   ]
        ]

        ax.scatter(nodes["x"],
                   nodes["y"],
                   color=colors,
                   marker="o",
                   label="LoRa Node")

        ax.scatter(gw["x"],
                   gw["y"],
                   color=gateway,
                   marker="s",
                   label="LoRa Gateway")

        ax.legend(loc='center left',
                  bbox_to_anchor=(0, -0.2),
                  ncol=4,
                  frameon=False,
                  handles=legend_elements)

        ax.set_xlim(BOUNDARIES.NODE_BOUNDARIES.X[0] - 10,
                    BOUNDARIES.NODE_BOUNDARIES.X[1] + 10)
        ax.set_ylim(BOUNDARIES.NODE_BOUNDARIES.Y[0] - 10,
                    BOUNDARIES.NODE_BOUNDARIES.Y[1] + 10)

        slicing = " with slicing" if source["config"][
            "sliceUse"] else " without slicing"
        nb_n = source["config"]["numberOfNodes"]
        nb_g = source["config"]["numberOfGateways"]
        z_r = source["config"]["zombieNodesPercentage"]
        r_r = source["config"]["reliableNodesPercentage"]

        ax.set_title(
            f"Map of nodes sf and gateways{slicing}\nn{nb_n} g{nb_g} z{z_r} r{r_r}"
        )

        fig.tight_layout()
        self.fig = fig


class SLICE_MAP:
    def __init__(self, data, source):
        fig, ax = plt.subplots(figsize=(10, 10), dpi=300)
        nodes = {
            "x": [int(n['x'][:-4]) for n in source['loRaNodes']],
            "y": [int(n['y'][:-4]) for n in source['loRaNodes']]
        }
        gw = {
            "x": [int(n['x'][:-4]) for n in source['gateways']],
            "y": [int(n['y'][:-4]) for n in source['gateways']]
        }

        node_slices = []

        for node in source["loRaNodes"]:
            node_slices.append(
                data["initialSlice"][f"initialSlice_{node['num']}"])

        colors = _color_coding(node_slices, "slices")

        legend_elements = [
            Line2D([0], [0],
                   marker='s',
                   color=gateway,
                   label='LoRa Gateway',
                   ls=""), *[
                       Line2D([0], [0],
                              marker='o',
                              color=slice_colors[node_slice],
                              label=f"{node_slice}",
                              ls="")
                       for node_slice in ["normal", "zombie", "reliable"]
                   ]
        ]

        ax.scatter(nodes["x"],
                   nodes["y"],
                   color=colors,
                   marker="o",
                   label="LoRa Node")

        ax.scatter(gw["x"],
                   gw["y"],
                   color=gateway,
                   marker="s",
                   label="LoRa Gateway")

        ax.legend(loc='center left',
                  bbox_to_anchor=(0, -0.2),
                  ncol=3,
                  frameon=False,
                  handles=legend_elements)

        ax.set_xlim(BOUNDARIES.NODE_BOUNDARIES.X[0] - 10,
                    BOUNDARIES.NODE_BOUNDARIES.X[1] + 10)
        ax.set_ylim(BOUNDARIES.NODE_BOUNDARIES.Y[0] - 10,
                    BOUNDARIES.NODE_BOUNDARIES.Y[1] + 10)

        slicing = " with slicing" if source["config"][
            "sliceUse"] else " without slicing"
        nb_n = source["config"]["numberOfNodes"]
        nb_g = source["config"]["numberOfGateways"]
        z_r = source["config"]["zombieNodesPercentage"]
        r_r = source["config"]["reliableNodesPercentage"]

        ax.set_title(
            f"Map of nodes sf and gateways{slicing}\nn{nb_n} g{nb_g} z{z_r} r{r_r}"
        )

        fig.tight_layout()
        self.fig = fig