import yaml
import matplotlib.pyplot as plt
from graphs.Plot import PDR_BY_SLICE_BOXPLOT, PDR_BY_ZOMBIE_NODE_PERCENTAGE
from graphs.Map import PDR_MAP, SF_MAP, SLICE_MAP

print("\nSTART OF DATA PLOTTING...")

data = yaml.safe_load(open("data/simulations.yml"))
data.sort(key=lambda d: d['nb_n'])

#region PLOT INDIVIDUAL SIMULATION DATA
# for d in data:
#     source = yaml.safe_load(open(f"simulations_sources/{d['name']}.yml"))
    
#     #region PDR BY SLICE
#     print(f"PLOTTING {d['name'].upper()} PDR BY SLICE...")
#     fig = PDR_BY_SLICE_BOXPLOT(d, source).fig
#     plt.savefig(f"images/{d['name']}-pdr-slice.png")
#     plt.close()
#     #endregion

#     #region MAP PDR
#     print(f"PLOTTING {d['name'].upper()} MAP...")
#     fig = PDR_MAP(d, source).fig
#     plt.savefig(f"images/{d['name']}-pdr-map.png")
#     plt.close()
#     #endregion

#     #region MAP FINAL SF
#     print(f"PLOTTING {d['name'].upper()} SF MAP...")
#     fig = SF_MAP(d, source).fig
#     plt.savefig(f"images/{d['name']}-sf-map.png")
#     plt.close()
#     #endregion

#     #region MAP SLICES
#     print(f"PLOTTING {d['name'].upper()} SLICES MAP...")
#     fig = SLICE_MAP(d, source).fig
#     plt.savefig(f"images/{d['name']}-slice-map.png")
#     plt.close()
#     #endregion

#endregion

#region DATA AGGREGATED
#region EVOLUTION BY ZOMBIE NODE PERCENTAGE
print(f"PLOTTING PDR EVOLUTION...")
fig = PDR_BY_ZOMBIE_NODE_PERCENTAGE(data).fig
plt.savefig(f"images/pdr-evolution.png")
plt.close()
#endregion


#endregion

print("END OF DATA PLOTTING.\n")