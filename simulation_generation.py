import os
import yaml
import numpy as np

from utils import BOUNDARIES, SLICES

_rng = np.random.default_rng()

NODE_X_BOUNDARIES = BOUNDARIES.NODE_BOUNDARIES.X
NODE_Y_BOUNDARIES = BOUNDARIES.NODE_BOUNDARIES.Y

GW_X_BOUNDARIES = BOUNDARIES.GW_BOUNDARIES.X
GW_Y_BOUNDARIES = BOUNDARIES.GW_BOUNDARIES.Y

NS_X_BOUNDARIES = BOUNDARIES.NS_BOUNDARIES.X
NS_Y_BOUNDARIES = BOUNDARIES.NS_BOUNDARIES.Y

GW_PORT = 2000
NS_PORT = 1000


def _create_xy(x_boundaries, y_boundaries, nb):
    x = _rng.integers(*x_boundaries, size=nb)
    y = _rng.integers(*y_boundaries, size=nb)

    coords = [(f"{int(x[i])}.00m", f"{int(y[i])}.00m") for i in range(nb)]

    return coords


def _create_gateways(numberOfGateways):
    gw_coords = _create_xy(GW_X_BOUNDARIES, GW_Y_BOUNDARIES, numberOfGateways)
    gws = []
    for i in range(numberOfGateways):
        gws.append({
            "num": i,
            "x": gw_coords[i][0],
            "y": gw_coords[i][1],
            "destPort": NS_PORT,
            "localPort": GW_PORT
        })
    return gws


def _create_network_servers(numberOfNetworkServers):
    ns_coords = _create_xy(NS_X_BOUNDARIES, NS_Y_BOUNDARIES,
                           numberOfNetworkServers)
    nss = []
    for i in range(numberOfNetworkServers):
        nss.append({
            "num": i,
            "x": ns_coords[i][0],
            "y": ns_coords[i][1],
            "destPort": GW_PORT,
            "localPort": NS_PORT
        })
    return nss


def _create_nodes(numberOfNodes, sf, slices_distribution, sliceUse):
    node_coords = _create_xy(NODE_X_BOUNDARIES, NODE_Y_BOUNDARIES,
                             numberOfNodes)
    node_slices = _rng.choice(["zombie", "reliable", "normal"],
                              p=[
                                  slices_distribution["zombie"],
                                  slices_distribution["reliable"],
                                  slices_distribution["normal"]
                              ],
                              size=numberOfNodes)
    nodes = []
    for i in range(numberOfNodes):
        initialSF = sf
        if sliceUse:
            if str(node_slices[i]) == "zombie":
                initialSF = _rng.choice(SLICES.ZOMBIE.SF)
            elif str(node_slices[i]) == "normal":
                initialSF = _rng.choice(SLICES.NORMAL.SF)
            elif str(node_slices[i]) == "reliable":
                initialSF = _rng.choice(SLICES.RELIABLE.SF)
        activateADR = "false" if sliceUse else "true"
        nodes.append({
            "num": i,
            "x": node_coords[i][0],
            "y": node_coords[i][1],
            "sf": int(initialSF),
            "activateADR": activateADR,
            "initialSlice": str(node_slices[i])
        })
    return nodes


def _name(sim):
    name = f"n{sim['numberOfNodes']}-gw{sim['numberOfGateways']}"
    name += f"{'-ADR' if sim['activateADR'] == True else ''}"
    name += f"-SF{sim['startingSF']}"
    name += f"-z{int(sim['zombieNodesPercentage'] * 100)}-r{int(sim['reliableNodesPercentage'] * 100)}"
    name += "-S" if sim["sliceUse"] else "-NS"
    return name


class Run:
    def __init__(self, run_config, global_config, gen_config):
        for key in global_config.keys():
            if key not in run_config:
                run_config[key] = global_config[key]
        for key in run_config.keys():
            if type(run_config[key]) == list:
                run_config[key] = run_config[key][:run_config["numberOfSims"]]
        self.config = run_config
        self.simulations = []
        for i in range(self.config["numberOfSims"]):
            sim_config = {}
            for key in self.config:
                if key != "numberOfSims":
                    if type(self.config[key]) != list:
                        sim_config[key] = self.config[key]
                    else:
                        sim_config[key] = self.config[key][i]
            self.simulations.append(Simulation(sim_config, gen_config))


class Simulation:
    def __init__(self, config, gen_config):
        self.name = _name(config)
        self.config = {"config": config}
        self.config["config"]["simulationName"] = self.name
        nodes_setup_sources = os.listdir("nodes_models")
        dist = f"z{self.config['config']['zombieNodesPercentage']*100}r{self.config['config']['reliableNodesPercentage']*100}_model"
        node_setup_source = [ns for ns in nodes_setup_sources if dist in ns]
        if len(node_setup_source) == 0 or gen_config["reuseNodes"] == False:
            self.config["gateways"] = _create_gateways(
                self.config["config"]["numberOfGateways"])
            
            slices_distribution = {
                "zombie": self.config["config"]["zombieNodesPercentage"],
                "reliable": self.config["config"]["reliableNodesPercentage"]
            }

            slices_distribution["normal"] = 1 - (
                slices_distribution["zombie"] +
                slices_distribution["reliable"])
            
            self.config["loRaNodes"] = _create_nodes(
                self.config["config"]["numberOfNodes"],
                self.config["config"]["startingSF"], slices_distribution,
                self.config["config"]["sliceUse"])
            
            self.config["networkServers"] = _create_network_servers(
                self.config["config"]["numberOfNetworkServers"])
            
            node_setup_source = {
                "loRaNodes": self.config["loRaNodes"],
                "gateways": self.config["gateways"],
                "networkServers": self.config["networkServers"]
            }
            with open(f"nodes_models/{dist}.yml", "w+") as f:
                f.write(yaml.dump(node_setup_source))
        else:
            with open(f"nodes_models/{node_setup_source[0]}", "r") as source:
                node_setup = yaml.safe_load(source.read())
                self.config["gateways"] = [
                    node_setup["gateways"][i]
                    for i in range(self.config["config"]["numberOfGateways"])
                ]
                self.config["loRaNodes"] = [
                    node_setup["loRaNodes"][i]
                    for i in range(self.config["config"]["numberOfNodes"])
                ]
                if self.config["config"]["sliceUse"] == False:
                    for i in range(self.config["config"]["numberOfNodes"]):
                        self.config["loRaNodes"][i]["sf"] = self.config["config"]["startingSF"]
                        self.config["loRaNodes"][i]["activateADR"] = "true" if self.config["config"]["activateADR"] else "false"

                self.config["networkServers"] = [
                    node_setup["networkServers"][i] for i in range(
                        self.config["config"]["numberOfNetworkServers"])
                ]

        self.config["config"]["activateADR"] = "true" if self.config["config"][
            "activateADR"] else "false"


print("\nGENERATING SIMULATIONS SOURCES FROM SIMULATION CONFIG...")

simulations_config = yaml.safe_load(open("config/simulations.yml"))
simulations = []

print(f"GENERATING {len(simulations_config['runs'])} RUNS...")

for run_config in simulations_config["runs"]:
    r = Run(run_config, simulations_config["global"],
            simulations_config["generation_config"])
    print("RUN PARAMETERS :")
    for p in r.config:
        print(f"\t{p}: {r.config[p]}")
    print("GENERATING SIMULATIONS USING RUN CONFIG...")
    for sim in r.simulations:
        print(f"\tGENERATED {sim.name}...")
        with open("simulations_sources/" + sim.name + ".yml", "w+") as f:
            f.write(yaml.dump(sim.config))
        simulations.append(sim.config["config"])

print("WRITING TO FILES...")

with open("tmp/simulations.yml", "w+") as f:
    f.write(yaml.dump(simulations, indent=1))

print("END OF GENERATION\n")
