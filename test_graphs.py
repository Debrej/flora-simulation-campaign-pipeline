import matplotlib.pyplot as plt
import numpy as np

plt.set_cmap("Pastel2")

labels = ["0.10", "0.20", "0.30"]
zombie_slicing_pdr = [70.0, 65.2, 62.3]
normal_slicing_pdr = [65.2, 73.4, 87.8]
reliable_slicing_pdr = [67.2, 85.2, 96.4]
zombie_no_slicing_pdr = [56, 58, 57]
normal_no_slicing_pdr = [65, 61, 57]
reliable_no_slicing_pdr = [65, 57, 48]

x = np.arange(len(labels))
width = 0.10

fig, ax = plt.subplots(figsize=(8,8))

ax.bar(x - 3 * (width) + width / 2, zombie_no_slicing_pdr, width, color="darkgrey", label="ZNS")
ax.bar(x - 2 * (width) + width / 2, zombie_slicing_pdr, width, color="lightgrey", label="ZS")
ax.bar(x - 1 * (width ) + width / 2, normal_no_slicing_pdr, width, color="darkblue", label="NNS")
ax.bar(x + 1 * (width) - width / 2, normal_slicing_pdr, width, color="lightblue", label="NS")
ax.bar(x + 2 * (width) - width / 2, zombie_no_slicing_pdr, width, color="darkorange", label="RNS")
ax.bar(x + 3 * (width) - width / 2, reliable_slicing_pdr, width, color="yellow", label="RS")

ax.legend()

ax.set_xticks(x, labels)

fig.tight_layout()

plt.savefig("test_graph.png")
plt.close()
