#!/bin/bash

mkdir -p tmp;

python3 setup.py;
source tmp/setup_variable.sh;

mkdir -p $CODE_FOLDER_PATH/backup/images;
mkdir -p $CODE_FOLDER_PATH/backup/logs;
mkdir -p $CODE_FOLDER_PATH/data;
mkdir -p $CODE_FOLDER_PATH/images;
mkdir -p $CODE_FOLDER_PATH/logs;
mkdir -p $CODE_FOLDER_PATH/nodes_models;
mkdir -p $CODE_FOLDER_PATH/out;
mkdir -p $CODE_FOLDER_PATH/simulations;
mkdir -p $CODE_FOLDER_PATH/simulations_sources;

rm $CODE_FOLDER_PATH/simulations/*;
rm $CODE_FOLDER_PATH/simulations_sources/*;
rm $CODE_FOLDER_PATH/images/*;
rm $CODE_FOLDER_PATH/tmp/*;
rm $CODE_FOLDER_PATH/logs/*;

python3 $CODE_FOLDER_PATH/simulation_generation.py;
python3 $CODE_FOLDER_PATH/template_generation.py;
python3 $CODE_FOLDER_PATH/run_simulations.py;
python3 $CODE_FOLDER_PATH/data_extraction.py;
python3 $CODE_FOLDER_PATH/data_visualization.py;

timestamp=$(date +%s);
tar czvf $CODE_FOLDER_PATH/backup/images/images_$timestamp.tar.gz $CODE_FOLDER_PATH/images/*;

python3 $CODE_FOLDER_PATH/report_generation.py;
