import re
import yaml
import numpy as np
from os import listdir
from os.path import isfile, join


def _create_regex_unique(string, res_type):
    if res_type == "number":
        res_type_str = "[0-9]+"
    elif res_type == "string":
        res_type_str = "\w+"
    pattern = f"{re.escape(string)} (?P<value>{res_type_str})\n"
    return re.compile(pattern)


def _create_regex_mean(string, res_type):
    if res_type == "number":
        res_type_str = "[0-9]+"
    elif res_type == "string":
        res_type_str = "\w+"
    arr_str = list(map(re.escape, string.split("[mean_param]")))
    string = arr_str[0] + "[0-9]+" + arr_str[1]
    pattern = "{string} (?P<value>{res_type_str})\n".format(
        string=string, res_type_str=res_type_str)
    return re.compile(pattern)


def _create_regex_multiple(string, res_type):
    if res_type == "number":
        res_type_str = "[0-9]+"
    elif res_type == "string":
        res_type_str = "\w+"
    arr_str = list(map(re.escape, string.split("[multiple_param]")))
    string = arr_str[0] + "(?P<num>[0-9]+)" + arr_str[1]
    pattern = "{string} (?P<value>{res_type_str})\n".format(
        string=string, res_type_str=res_type_str)
    return re.compile(pattern)


class Parameter:
    def __init__(self, name, type, string, param, res_type):
        self.name = name
        self.type = type
        self.values = []
        self.average = 0
        self.param = param
        self.res_type = res_type

        if self.type == "unique":
            self.regex = _create_regex_unique(string, res_type)
        elif self.type == "mean":
            self.regex = _create_regex_mean(string, res_type)
        elif self.type in ["multiple", "all"]:
            self.regex = _create_regex_multiple(string, res_type)

    def compute_param(self, log):
        if self.type == "unique":
            self.values.append(int(self.regex.search(log).group("value")))

        elif self.type == "mean":
            val = []
            for m in self.regex.finditer(log):
                val.append(int(m.group("value")))
            self.values.append(int(np.sum(val)))

        elif self.type in ["multiple", "all"]:
            val = []
            for m in self.regex.finditer(log):
                val.append({"num": m.group("num"), "value": m.group("value")})
            self.values.append(val)

    def consolidate_param(self, nb_n, nb_gw):
        if self.type in ["unique", "mean"]:
            self.average = float(np.mean(self.values)) / nb_n
        elif self.type in ["multiple", "all"]:
            nums = [str(i) for i in range(self.param)]
            temp_values = {}
            temp_extended_values = []
            for v in self.values:
                temp_extended_values.extend(v)
            for num in nums:
                try:
                    if self.res_type == "number":
                        if self.type == "multiple":
                            temp_values[f"{self.name}_{num}"] = float(
                                np.array([
                                    float(v['value']) for v in temp_extended_values
                                    if v['num'] == num
                                ]).mean())
                        elif self.type == "all":
                            temp_values[f"{self.name}_{num}"] = [
                                    float(v['value']) for v in temp_extended_values
                                    if v['num'] == num
                                ]
                    elif self.res_type == "string":
                        temp_values[f"{self.name}_{num}"] = [
                            v['value'] for v in temp_extended_values
                            if v['num'] == num
                        ][0]
                except KeyError:
                    temp_values[f"{self.name}_{num}"] = 0
                except ValueError:
                    if self.res_type == "number":
                        temp_values[f"{self.name}_{num}"] = 0
            self.values = temp_values


class Simulation:
    def __init__(self, sim, parameters):
        self.name = sim["simulationName"]
        self.numberOfNodes = sim["numberOfNodes"]
        self.numberOfGateways = sim["numberOfGateways"]
        self.parameters = [
            Parameter(p["name"], p["type"], p["str"],
                      sim[p["param"]] if p["param"] != '' else "",
                      p["res_type"]) for p in parameters
        ]
        self.files = []

    def compute_metrics(self):
        if len(self.files) != 0:
            for log_file in self.files:
                with open(join(log_directory, log_file), "r") as f:
                    log = f.read()
                    log = log.replace('"\\"', '').replace('\\""', '')
                    for param in self.parameters:
                        param.compute_param(log)

            for param in self.parameters:
                param.consolidate_param(self.numberOfNodes,
                                        self.numberOfGateways)

    def extract_data(self):
        ret = {
            "name": self.name,
            "nb_n": self.numberOfNodes,
            "nb_gw": self.numberOfGateways
        }
        for param in self.parameters:
            if param.type in ["unique", "mean"]:
                ret[param.name] = param.average
            else:
                ret[param.name] = param.values
        return ret


print("\nEXTRACTING DATA FROM SIMULATIONS LOGS...")

#region PARAMETERS

parameters_config = yaml.safe_load(open("config/parameters.yml"))
simulations_config = yaml.safe_load(open("tmp/simulations.yml"))

simulations = [Simulation(s, parameters_config) for s in simulations_config]

#endregion

#region LOG FILES

print("LOADING LOGS FILES...")

log_directory = "logs/"

log_files = [
    f for f in listdir(log_directory) if isfile(join(log_directory, f))
]

for sim in simulations:
    name = r"{sim.name}-s[0-9]+.sca".format(sim=sim)
    for f in log_files:
        if re.match(name, f):
            sim.files.append(f)

#endregion

#region SEARCH PARAMETERS

print("COMPUTING METRICS...")

for sim in simulations:
    sim.compute_metrics()

#endregion

#region EXTRACT DATA

print("EXTRACT DATA TO FILE...")

data = []

for sim in simulations:
    print(f"{sim.name}")
    for param in sim.parameters:
        print(f"{param.name}: {param.average}")
    data.append(sim.extract_data())

with open("data/simulations.yml", "w+") as f:
    f.write(yaml.dump(data, indent=1))

#endregion

print("END OF DATA EXTRACTION...\n")
