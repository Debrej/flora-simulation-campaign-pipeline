import yaml
from jinja2 import Template

print("\nSETTING VARIABLES UP...")

setup_config = yaml.safe_load(open("config/setup.yml"))
template_file = "templates/setup_variables.j2"
setup_file = "tmp/setup_variable.sh"

with open(template_file) as file:
    template = Template(file.read())

with open(setup_file, "w") as file:
    file.write(template.render(setup_config))

print("END OF SETUP.\n")